import './App.scss';

//ROUTER 
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

//COMPONENTS
import Header from './Components/Header/Header';
import { Footer } from './Components/Footer/Footer';
import { ClassRoom } from './Components/ClassRoom/ClassRoom';

//PAGES
import { HomePage } from './Pages/HomePage/HomePage';
import { AboutPage } from './Pages/AboutPage/AboutPage';
import CursesPage from './Pages/CursesPage/CursesPage';
import { RegisterPage } from './Pages/RegisterPage/RegisterPage';
import LoginPage from './Pages/LoginPage/LoginPage';
import { PrivatePage } from './Pages/PrivatePage/PrivatePage';
import CoursePage from './Pages/CoursePage/CoursePage';

//FUNCTIONS
import { connect } from 'react-redux';
import { ActivitiesPage } from './Pages/ActivitiesPage/ActivitiesPage';
import { ContactPage } from './Pages/ContactPage/ContactPage';


function App(props) {


  return (
    <div className="app">
      <BrowserRouter>
      <Header />
        <Routes>
            <Route path= "/" element={<HomePage />} />
            <Route path="aboutus" element={<AboutPage />}/>
            <Route path="courses" >
              <Route index element={<CursesPage/>}/>
              <Route path=":course" element={<CoursePage/>}/>
            </Route>
            <Route path="register" element={<RegisterPage/>}/>
            <Route path="login" element={<LoginPage/>}/>
            <Route path="contact" element={<ContactPage/>}/>
            <Route path="aula" element={<PrivatePage user={props.user}><ClassRoom/></PrivatePage>}/>
            <Route path="activities" element={<PrivatePage user={props.user}><ActivitiesPage/></PrivatePage>}/>
          </Routes>
        <Footer/>
      </BrowserRouter>
    </div>
  );
}

const mapStateToProps = (state) => ({
  user: state.auth.user
})

export default  connect(mapStateToProps)(App);
