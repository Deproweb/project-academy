import './Carousel.scss'

import Carousel from 'react-bootstrap/Carousel'

export const Carrousel = () => {
    return (<div className="c-carousel">
    <Carousel className="c-carousel__div">
        <Carousel.Item>
            <img
            className="d-block w-100"
            src="https://cdn.pixabay.com/photo/2017/08/01/00/38/man-2562325_1280.jpg"
            alt="First slide"
            />
            <Carousel.Caption className="c-carousel__div__caption">
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
            <img
            className="d-block w-100"
            src="https://cdn.pixabay.com/photo/2021/01/21/15/54/books-5937716_1280.jpg"
            alt="Second slide"
            />
            <Carousel.Caption className="c-carousel__div__caption">
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
            <img
            className="d-block w-100"
            src="https://cdn.pixabay.com/photo/2017/07/31/11/21/people-2557399_1280.jpg"
            alt="Third slide"
            />
            <Carousel.Caption className="c-carousel__div__caption">
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </Carousel.Caption>
        </Carousel.Item>
    </Carousel>
    </div>);
};
