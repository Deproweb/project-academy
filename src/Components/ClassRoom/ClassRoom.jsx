import './ClassRoom.scss'

import Nav from 'react-bootstrap/Nav'
import { Link} from 'react-router-dom';
import ProgressBar from 'react-bootstrap/ProgressBar'

export const ClassRoom = () => {
    return (
        <div className="c-classroom">
            <div className="c-classroom__subnav">
                <Nav className="justify-content-center c-header__nav" activeKey="/home">
                        <Nav.Item>
                            <Link to="/aula" className="c-header__nav__link">Mi curso</Link> 
                        </Nav.Item>
                        <Nav.Item>
                            <Link to="/activities" className="c-header__nav__link">Actividades</Link> 
                        </Nav.Item>
                    </Nav>
            </div>
            <div className="c-classroom__course">
                <img className="c-classroom__course__img" src="https://image.freepik.com/vector-gratis/aula-vacia-interior-escuela-o-clase-universitaria_107791-631.jpg" alt="" />
                <div className="c-classroom__course__own">
                    <h4><b>Mi curso:</b> Curso de escritura creativa </h4>
                    <h5><b>Horas:</b> 25</h5>
                    <h5>Progreso:</h5>
                    <ProgressBar variant="warning" now={25} />
                </div>
            </div>
        </div>
    );
};
