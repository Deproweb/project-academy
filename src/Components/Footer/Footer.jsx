import './Footer.scss';
import { AiOutlineMail } from 'react-icons/ai';
import { BsFillTelephoneFill, BsInstagram, BsFacebook, BsTwitter } from 'react-icons/bs';
import { FormattedMessage } from 'react-intl';

export const Footer = () => {
    return (<div className="c-footer">
        <div className="c-footer__div">
            <h5><FormattedMessage
                id="Footer.tlf"
            /></h5>
            <p> <BsFillTelephoneFill/> 0123456789</p>
        </div>
        <div className="c-footer__div">
        <h5><FormattedMessage
                id="Footer.email"
            /></h5>
            <p> <AiOutlineMail/> upgrade-academy@gmail.com</p>
        </div>
        <div className="c-footer__div">
        <h5><FormattedMessage
                id="Footer.follow"
            /></h5>
            <p><BsInstagram/>    <BsFacebook/>  <BsTwitter/></p>
        </div>
    </div>)
};
