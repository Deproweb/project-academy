import './FormRegister.scss';

import { useForm } from "react-hook-form";
import { RegisterUser } from '../../redux/actions/authAction';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router';

const FormRegister = (props) => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    
    const navigate = useNavigate()

    const onSubmit = data => {
        props.dispatch(RegisterUser(data))
        console.log(data);
        navigate("/")

        if (props.user) {
            navigate("/")
        }
    };

    return (

    <div className="c-form">

        <h3>Registrarse</h3>

        <form className="c-form__form" onSubmit={handleSubmit(onSubmit)}>


            <input className="c-form__form__input" placeholder="UserName"  {...register("username", { required: true })} />
            {errors.exampleRequired && <span>This field is required</span>}
            
            <input className="c-form__form__input" placeholder="Email" {...register("email", { required: true })} />
            {errors.exampleRequired && <span>This field is required</span>}
            
            <input className="c-form__form__input" type="password" placeholder="Password" {...register("password", { required: true })} />
            {errors.exampleRequired && <span>This field is required</span>}

            <input className="c-form__form__button" value= "Registrarme" type="submit"/>

        </form>
        <div>
            {props.error && <h4>{props.error}</h4>}    
        </div>  
    </div>
)};

const mapStateToProps = ( state ) => ({
    user: state.auth.user,
    error: state.auth.error
})

export default connect(mapStateToProps)(FormRegister)