import './Header.scss'
import { Link, useNavigate } from 'react-router-dom';
import { useContext } from 'react';

import {BsWhatsapp} from 'react-icons/bs'
import {  Logout } from '../../redux/actions/authAction';
import { connect } from 'react-redux';
import { LanguageContext } from '../../context/LanguageContext';

//Bootstrap
import Button from 'react-bootstrap/Button'
import Nav from 'react-bootstrap/Nav'
import { FormattedMessage } from 'react-intl';

export const Header = (props) => {

    const navigate = useNavigate()

    const {local, changeLanguage} = useContext(LanguageContext);

    return (<>
            <div className="c-subheader">
                <div className="c-subheader__log">
                <p> <BsWhatsapp/> ¿Quieres hablar por whastapp? Estamos conectados de 9:00 a 17:00</p>            
                </div>
                <div className="c-subheader__log">
                {!props.user && <Link className="c-subheader__log__link" to="/login"><FormattedMessage id="Header.log" /></Link>}
                {props.user && <h5 onClick={()=>props.dispatch(Logout())} className="c-subheader__log__par"><FormattedMessage id="Header.out" /></h5>}
                <Link to="/register" className="c-subheader__log__link"><FormattedMessage id="Header.reg" /></Link> 
                {local === 'es' && <Button variant="light" className="c-subheader__button" onClick={()=>changeLanguage('en')}>English</Button>}
                {local === 'en' && <Button variant="light" className="c-subheader__button" onClick={()=>changeLanguage('es')}>Español</Button>}
                </div>
            </div>    
            <div className="c-header">
                <div className="c-header__logo">
                    <img onClick={() => navigate("/")} className="c-header__logo__img" src="https://cdn.discordapp.com/attachments/846417268506099735/940158147123871846/Upgrade.png" alt="logo" />
                </div>
                <Nav className="justify-content-center c-header__nav" activeKey="/home">
                    <Nav.Item>
                        <Link to="/" className="c-header__nav__link">Home</Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/aboutus" className="c-header__nav__link"><FormattedMessage id="Header.who" /></Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/courses" className="c-header__nav__link"><FormattedMessage id="Header.Courses" /></Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/aula" className="c-header__nav__link"><FormattedMessage id="Header.class" /></Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/contact" className="c-header__nav__link"><FormattedMessage id="Header.contact" /></Link> 
                    </Nav.Item>
                </Nav>
            </div>
            </>)
};

const mapStateToProps = (state) => ({

    user: state.auth.user

})

export default connect(mapStateToProps)(Header)