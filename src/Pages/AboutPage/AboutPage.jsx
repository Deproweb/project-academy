import './AboutPage.scss'

export const AboutPage = () => {
    return (<div className="c-about">
        <div className="c-about__title">
            <div className="c-about__title__tit">
            {/* <h1>Acerca de Upgrade Academy</h1> */}
            </div>
        </div>
        <div className="c-about__div">
            <img src="https://image.freepik.com/vector-gratis/conjunto-iconos-e-learning_98292-716.jpg" alt="" />
            <div className="c-about__div__text">
                <h3>¿Quiénes somos?</h3>
                <p>Upgrade Academy es una academia online que ofrece diferentes cursos online. Contamos con 12 años de experiencia en el sector de la enseñanza</p>
            </div>
        </div>
        <div className="c-about__div">
        <img src="https://image.freepik.com/vector-gratis/icono-educacion-linea-plano_1284-5051.jpg" alt="" />
            <div className="c-about__div__text">
                <h3>¿Qué hacemos?</h3>
                <p>Podrás hacerlo todo de manera online, ya que nuestro objetivo es adaptarnos a tu ritmo de estudio y hacer que te sientas acompañado durante todo el proceso. </p>
            </div>
        </div>
        <div className="c-about__div">
            <img src="https://image.freepik.com/vector-gratis/conjunto-personas-universidad_1284-40689.jpg" alt="" />
            <div className="c-about__div__text">
                <h3>¿Cómo funcionamos?</h3>
                <p>Pondremos a disposición todas las herramientas necesarias para que tu aprendizaje sea lo más provechoso posible. Además contarás con un campus virtual donde podrás hacer un seguimiento de todos los ejercicios</p>
            </div>
        </div>
    </div>);
};
