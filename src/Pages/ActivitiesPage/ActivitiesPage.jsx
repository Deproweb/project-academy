import './Activities.page.scss'
import './../../Components/ClassRoom/ClassRoom.scss'
import Button from 'react-bootstrap/Button'

import Nav from 'react-bootstrap/Nav'
import { Link} from 'react-router-dom';

export const ActivitiesPage = () => {
    return (
        <>
            <div className="c-classroom__subnav">
                <Nav className="justify-content-center c-header__nav" activeKey="/home">
                        <Nav.Item>
                            <Link to="/aula" className="c-header__nav__link">Mi curso</Link> 
                        </Nav.Item>
                        <Nav.Item>
                            <Link to="/activities" className="c-header__nav__link">Actividades</Link> 
                        </Nav.Item>
                    </Nav>
            </div>
            <div className="c1">
                <img src="https://img.freepik.com/vector-gratis/estudiante-laptop-estudiando-curso-linea_74855-5293.jpg?w=1800" alt="" />
                <div className="c1__activities">
                    <div className="c1__activities__div">
                        <img src="https://cdn.pixabay.com/photo/2018/09/24/15/04/board-3700116_1280.jpg" alt="" />
                        <div>
                            <h3>Actividad 1</h3>
                            <h4>Entregada</h4>
                            <h5>22/02/2022</h5>
                            <Button variant="success">Ir a la actividad</Button>
                        </div>
                    </div>
                    <div className="c1__activities__div">
                    <img className src="https://cdn.pixabay.com/photo/2018/09/24/15/04/board-3700116_1280.jpg" alt="" />
                        <div>
                            <h3>Actividad 2</h3>
                            <h4>Por entregar</h4>
                            <h5>28/02/2022</h5>
                            <Button variant="success">Ir a la actividad</Button>
                        </div>
                    </div>
                    <div className="c1__activities__div">
                    <img src="https://cdn.pixabay.com/photo/2018/09/24/15/04/board-3700116_1280.jpg" alt="" />
                        <div>
                            <h3>Actividad 3</h3>
                            <h4>Por entregar</h4>
                            <h5>06/03/2022</h5>
                            <Button variant="success">Ir a la actividad</Button>
                        </div>
                    </div>
                </div>
        </div>
        </>
    );
}
