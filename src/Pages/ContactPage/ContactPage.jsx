import './ContactPage.scss'
import { useForm } from "react-hook-form";
import Button from 'react-bootstrap/Button'


export const ContactPage = () => {

    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);
    
    return (

        <>

        <h3 className="c-contact__title">
            Formulario de contacto
        </h3>

        <form className="c-contact" onSubmit={handleSubmit(onSubmit)}>
        
        <input className="c-contact__input" placeholder="Nombre" {...register("nombre")} />

        <input className="c-contact__input" placeholder="Email" {...register("email", { required: true })} />
        {errors.exampleRequired && <span>This field is required</span>}
        
        <input className="c-contact__input__course" placeholder="Curso Elegido" {...register("course", { required: true })} />
        {errors.exampleRequired && <span>This field is required</span>}

        <Button variant="warning">Contactar</Button>

        </form>

        </>
    );
}
