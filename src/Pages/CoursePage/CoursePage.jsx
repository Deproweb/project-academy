import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router';
import './CoursePage.scss'

const CoursePage = (props) => {
    
    const {course} = useParams('course')   

    const [courseFound, setCourseFound] = useState({});

    useEffect(() => {
        props.list.map(element => {
            if(element.name === course) {
                setCourseFound(element)
            }
            return ""
        });
    }, [props.list, course] );

    console.log(courseFound)

    return (<div className="c-course">
            <h3 className="c-course__title">{courseFound.name}</h3>
            <img src={courseFound.img} alt="Imagen del curso" />
            <div className="c-course__text">
            <h5>{courseFound.description}</h5>
            <p> Horas : {courseFound.horas}</p>
            <p> Precio: {courseFound.price} €</p>
            <p> Fecha: {courseFound.date}</p>
            </div>
    </div>);
};

const mapStateToProps = (state) => ({
    
    list : state.api.courses

})

export default connect(mapStateToProps)(CoursePage)