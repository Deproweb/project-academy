import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { callToApi } from '../../redux/actions/apiActions';
import { useNavigate } from 'react-router';

//Boostrap
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

import './CursesPage.scss'

const CursesPage = (props) => {

    //Hooks
    const navigate = useNavigate()

    useEffect(() => {
        props.dispatch(callToApi())

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    //Card
    const sendInfo = (animal) => {
        navigate(`/courses/${animal.name}`)
    };


    //Filter

    const [hasFilteredResults, setHasFilteredResults] = useState(false); //Variable de control
    //const [error, setError] = useState(''); Variable de error
    const [filteredResults, setFilteredResults] = useState([]); //Variable que almacena


    //Esta función se encarga de llamar a la otra con el valor correspondiente
    const onChangeValue = (ev) => {
        if(!ev.target.value){
            setHasFilteredResults(false);
            setFilteredResults([]);
        }
        else filterSignatures(ev.target.value)
    }
    
    const filterSignatures = (value) => {
        const results = props.courses.filter(course => course.name.toLowerCase().includes(value.toLowerCase()));
        setFilteredResults(results);
        if (results.length) {
            setHasFilteredResults(true)
        }
        if (results.length === 0) {
            setHasFilteredResults(false);
            setFilteredResults([]);
        }
    };

    return (<div className="c-cursespage">

        <div className="c-cursespage__input">
            <label htmlFor="signature" className="c-cursespage__input__lab">Busca por materia</label>
            <input name="signature" className="c-cursespage__input__inp" type="text" onChange={onChangeValue} />
        </div>

        {hasFilteredResults && filteredResults.map(result =>
                <Card className="c-cursespage__card" key={result.name} bg="success" style={{ width: '18rem', height: '450px' , margin: '15px', color: 'white'}}>
                <Card.Img variant="top" src={result.img} />
                <Card.Body>
                <Card.Title className="c-cursespage__card__title">{result.name}</Card.Title>
                <Card.Text className="c-cursespage__card__subtitle">
                    {result.description}
                </Card.Text>
                <Button onClick={()=>sendInfo(result)} className="c-cursespage__card__button" variant="warning">Más info</Button>
                </Card.Body>
            </Card>
            )}

        {!hasFilteredResults && props.courses.map(course => 
            <Card className="c-cursespage__card" key={course.name} bg="success" style={{ width: '18rem', height: '450px' , margin: '15px', color: 'white'}}>
                <Card.Img variant="top" src={course.img} />
                <Card.Body>
                <Card.Title className="c-cursespage__card__title">{course.name}</Card.Title>
                <Card.Text className="c-cursespage__card__subtitle">
                    {course.description}
                </Card.Text>
                <Button onClick={()=>sendInfo(course)} className="c-cursespage__card__button" variant="warning">Más info</Button>
                </Card.Body>
            </Card>
        )}
    </div>);
};

const mapStateToProps = (state) => ({

    courses: state.api.courses

})

export default connect(mapStateToProps)(CursesPage)
