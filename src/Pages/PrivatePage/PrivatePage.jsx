import './PrivatePage.scss'

export const PrivatePage = ({user, children}) => {

    if (!user) 
    return <div className = "notLogin">
        <div className="notLogin__div">
            <h3 className="notLogin__div__title">Necesitas estar registrado o logueado para entrar en el aula virtual</h3>
        </div>
    </div>;

    if (user) 
    return children
};
