import { createContext, useState } from "react";
import { IntlProvider } from "react-intl";
import spanish from '../lang/es.json';
import english from '../lang/en.json';

export const LanguageContext = createContext();

const locale = 'es';
let lang;

switch (locale) {
    case 'es': {
        lang = spanish;
        break;
    }
    case 'en': {
        lang = english;
        break;
    }
    default:
        lang = spanish
};

const LanguageWrapper = (props) => {

    const [local, setLocal] = useState(locale);
    const [messages, setMessages] = useState(lang);

    const changeLanguage = (newLocale) => {
        setLocal(newLocale);

        if (newLocale === 'en') setMessages(english);
        if (newLocale === 'es') setMessages(spanish);
    };

    return (
        <LanguageContext.Provider value={{local, changeLanguage}}>
            <IntlProvider locale={local} default='es' messages={messages}>
                {props.children}
            </IntlProvider>
        </LanguageContext.Provider>
    )
};

export default LanguageWrapper;