import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

//Redux

import {Provider} from 'react-redux';
import { store } from './redux/store/store';
import LanguageWrapper from './context/LanguageContext';

ReactDOM.render(

  <React.StrictMode>
    <LanguageWrapper>
      <Provider store={store}>
        <App />
      </Provider>
    </LanguageWrapper>
  </React.StrictMode>,
  document.getElementById('root')
);
