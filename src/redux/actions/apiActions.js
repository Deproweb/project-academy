import axios from "axios"

export const GET_COURSES = "[API] getCourses";

//Actions

const getCourses = (data) => ({

    type: GET_COURSES,
    payload: data

});

//Esto es lo que exportamos

export const callToApi = () => {

    return async (dispatch) => {

        const res = await axios.get('https://my-json-server.typicode.com/DeproWeb/curses/db')
        const data = res.data.courses
        dispatch(getCourses(data))
        console.log(data);
    }
};