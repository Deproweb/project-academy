//Esto son simplementes strings.
export const REGISTER = "[AUTH] register";
export const LOGIN = "[AUTH] login";
export const LOGOUT = "[AUTH] logout";
export const REGISTER__ERROR = "[AUTH] register error"
export const LOGIN__ERROR = "[AUTH] login error"

//Acciones

const authLogin = (data) => ({

    type: LOGIN,

    payload: data

})

const authRegister = (data) => ({

    type: REGISTER,

    payload: data

})

const authLogout = () => ({

    type: LOGOUT,

})


//Esto es lo que se exporta, las funciones: 
//(Tú puedes hacer lo que quieras aquí dentro, siempre y cuando luego uses los dispatch que llaman al reducer)

export const RegisterUser = (data) => {
    return async (dispatch) =>{ //Esto se hace siempre para entrar con el dispatch

        const request = await fetch("http://localhost:4000/auth/register", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
            },
            credentials: "include",
            body: JSON.stringify(data),
            });
            
        const result = await request.json();

        if (request.ok) {
            dispatch(authRegister(result))
        } else {
            dispatch({ type: REGISTER__ERROR, payload: false });
        }
    }
}


export const LoginUser = (data) => {

    return async (dispatch) => {
    
        const request = await fetch("http://localhost:4000/auth/login", {
            method: "POST",
            headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            },
            credentials: "include",
            body: JSON.stringify(data),
        });
    
        const result = await request.json();

        if (request.ok) {
            dispatch(authLogin(result))
        } else {
            console.log(result.message);
            dispatch({ type: LOGIN__ERROR, payload: result.message });
        }
}
}

export const Logout = () => {
    return (dispatch) =>{
    
    dispatch(authLogout());

    }
}