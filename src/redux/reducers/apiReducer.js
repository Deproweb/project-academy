import { GET_COURSES } from "../actions/apiActions"


const INITIAL_STATE = ({
    courses: []
})

export const apiReducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case GET_COURSES:
            return {
                ...state,
                courses: action.payload
            }

        default: 
            return state

    }

}