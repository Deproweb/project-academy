import { LOGIN, LOGIN__ERROR, LOGOUT, REGISTER, REGISTER__ERROR } from "../actions/authAction"



const INITIAL_STATE = {

    user: null,
    error: null

}

export const authReducer = (state = INITIAL_STATE, action ) => {

    switch (action.type) {

        case LOGIN: 
            return {
            ...state,
            user: action.payload
        }

        case (LOGIN__ERROR): {
            return {
                ...state, 
                error: action.payload, 
                user: null};
        }

        case REGISTER:
            return {
                user: action.payload
            }

        case (REGISTER__ERROR): {
            return {
                ...state, 
                error: action.payload, 
                user: null};
        }
    
        case LOGOUT:
            return {}    

        default:
            return state

    }
}